package net.bytesly.ug_androidmidtermtasktwo;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class UserFeedbackDbHelper extends SQLiteOpenHelper  {

    private static final String DATABASE_NAME = "DB_NAME";
    private static final int VERSION = 1;

    private static final String SQL_CREATE_TABLE = "CREATE TABLE " + UserFeedbackContract.TABLE_NAME + " ("
            + UserFeedbackContract.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + UserFeedbackContract.FIRST_NAME + " TEXT, "
            + UserFeedbackContract.LAST_NAME + " TEXT, "
            + UserFeedbackContract.EMAIL + " TEXT, "
            + UserFeedbackContract.REVIEW + " TEXT)";

    private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + UserFeedbackContract.TABLE_NAME;

    public UserFeedbackDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);
    }

    public void insert(String firstName, String lastName, String email, String review) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(UserFeedbackContract.FIRST_NAME, firstName);
        contentValues.put(UserFeedbackContract.LAST_NAME, lastName);
        contentValues.put(UserFeedbackContract.EMAIL, email);
        contentValues.put(UserFeedbackContract.REVIEW, review);

        getWritableDatabase().insert(UserFeedbackContract.TABLE_NAME, null, contentValues);

    }

    public int update(long id, String newReview) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(UserFeedbackContract.REVIEW, newReview);

        String where = UserFeedbackContract.ID + " = ?";
        String[] args = new String[]{
                String.valueOf(id)
        };

        return getWritableDatabase().update(
                UserFeedbackContract.TABLE_NAME,
                contentValues,
                where,
                args
        );

    }

    public int delete(long fbId) {

        String where = UserFeedbackContract.ID + " = ?";
        String[] args = new String[]{
                String.valueOf(fbId)
        };

        return getWritableDatabase().delete(UserFeedbackContract.TABLE_NAME, where, args);

    }

    public int deleteAll() {
        return getWritableDatabase().delete(UserFeedbackContract.TABLE_NAME, null, null);
    }

    public List<UserFeedback> select(String email) {

        String[] projection = new String[]{
                UserFeedbackContract.FIRST_NAME, UserFeedbackContract.LAST_NAME,
                UserFeedbackContract.EMAIL, UserFeedbackContract.REVIEW
        };

        String where = UserFeedbackContract.EMAIL + " = ?";
        String[] args = new String[]{
                email
        };

        String ordering = UserFeedbackContract.FIRST_NAME + " ASC";

        @SuppressLint("Recycle") Cursor cursor = getReadableDatabase().query(
                UserFeedbackContract.TABLE_NAME,
                projection,
                where,
                args,
                null,
                null,
                ordering
        );

        List<UserFeedback> feeds = new ArrayList<>();

        while (cursor.moveToNext()) {
            feeds.add(new UserFeedback(
                    cursor.getString(cursor.getColumnIndex(UserFeedbackContract.FIRST_NAME)),
                    cursor.getString(cursor.getColumnIndex(UserFeedbackContract.LAST_NAME)),
                    cursor.getString(cursor.getColumnIndex(UserFeedbackContract.EMAIL)),
                    cursor.getString(cursor.getColumnIndex(UserFeedbackContract.REVIEW))
            ));
        }

        return feeds;

    }

}
