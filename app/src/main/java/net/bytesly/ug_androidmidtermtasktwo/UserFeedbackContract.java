package net.bytesly.ug_androidmidtermtasktwo;

public class UserFeedbackContract {

    public static final String TABLE_NAME = "FEEDBACK";

    public static final String ID = "ID";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String EMAIL = "email";
    public static final String REVIEW = "review";

}
