package net.bytesly.ug_androidmidtermtasktwo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText editTextFirstName;
    EditText editTextLastName;
    EditText editTextEmail;
    EditText editTextReview;

    Button buttonSend;

    UserFeedbackDbHelper feedbackDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initFields();
        registerListeners();

        List<UserFeedback> select = feedbackDbHelper.select("example@mail.com");
        for (int i = 0; i < select.size(); i++) {
            Log.d("MyFeedbackList", select.get(i).toString());
        }
    }

    private void initFields() {
        editTextFirstName = findViewById(R.id.editTextFirstName);
        editTextLastName = findViewById(R.id.editTextLastName);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextReview = findViewById(R.id.editTextReview);

        buttonSend = findViewById(R.id.buttonSend);

        feedbackDbHelper = new UserFeedbackDbHelper(this);
    }

    private void registerListeners() {
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String validationResult = validateFields();

                if(!TextUtils.isEmpty(validationResult)) {
                    Toast.makeText(MainActivity.this,
                    "The following fields have mistakes:\n" + validationResult,
                    Toast.LENGTH_LONG).show();

                    return;
                }

                String newFirstName = editTextFirstName.getText().toString().trim();
                String newLastName = editTextLastName.getText().toString().trim();
                String newEmail = editTextEmail.getText().toString().trim();
                String newReview = editTextReview.getText().toString().trim();

                feedbackDbHelper.insert(newFirstName, newLastName, newEmail, newReview);

                Toast.makeText(MainActivity.this, "Thanks for your feedback!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String validateFields() {

        String checkResult = "";

        if (editTextFirstName.getText().toString().trim().length() < 2) {
            checkResult += "First name\n";
        }

        if (editTextLastName.getText().toString().trim().length() < 3) {
            checkResult += "Last name\n";
        }

        if (TextUtils.isEmpty(editTextEmail.getText()) ||
                !editTextEmail.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")) {
            checkResult += "Email\n";
        }

        if (editTextReview.getText().toString().trim().length() < 5) {
            checkResult += "Review";
        }

        return checkResult;

    }
}