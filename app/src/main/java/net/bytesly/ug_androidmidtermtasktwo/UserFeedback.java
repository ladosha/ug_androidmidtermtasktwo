package net.bytesly.ug_androidmidtermtasktwo;

public class UserFeedback {

    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private String review;


    public UserFeedback() {
    }

    public UserFeedback(Integer id, String firstName, String lastName, String email, String review) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.review = review;
    }

    public UserFeedback(String firstName, String lastName, String email, String review) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.review = review;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    @Override
    public String toString() {
        return "UserFeedback{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", review='" + review + '\'' +
                '}';
    }
}
